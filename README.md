# CPM.Vidly

## How to run the application

- Navigate to project root / where package.json is and run **npm install**
- After packages finish installing, run **npm start**

## Technologies Used

- Angular 7.0.2
- Typescript 3.1.6
- Rxjs 6
- Scss
- Bootstrap 4
- FontAwesome 5
- NgrxStore 6

## Application Info

### Routes
- `/home`
- `/movies`
- `/movies/:key`
- `/errors/404`

### Shell Component (App Layout)
- Each route in the app uses the Shell component as a layout for the app
- This includes:
	- Top navigation
		- Navbar component that is used to navigate between routes
		- **Note:** There are some dummy routes for the sake of populating the navbar, these will redirect you to the 404 page if clicked (Only Home and Movies nav links work, rest are dummies)
	- Breadcrumb and heading bar
		- Heading is an `<h1>` element (helps with SEO to have this on each route)
		- Can set a title and breadcrumb items easily for each route using their routing module file (or manually using `ShellService`)
			- See the routing files (Eg: `movies-routing.module.ts`) to see how the window title / heading / breadcrumb items are set (also see `AppRouteData` model in the src/core/route folder)
			- Also see `app.component.ts` to see how to listen to route changes and to retrieve data after each route change easily
	- Footer
		- Simple generic footer. Nothing much to see other than styling and responsiveness.
		- **Note:** Also has dummy links like the navbar for the sake of populating the footer with items.
	- Content container for route content background and placement
		- Background image set for route content, could also easily use a different background for each route using `[style.backgroundImage]` and route data with this setup but went with one background.
		- Handles positioning of route content using flexbox to make it easier to position new routes.

### Home Page
- Route: `/home`
- Breadcrumb items: `[NONE]`
- Window title: `Home`

- Shows the currently top 5 rated movies
- Can click on a movie's image or its name to navigate to its detail page


### Movie Details
- Route: `/movies/:key`
- Breadcrumb items: `[Home > Movies]`
- Window title: `Movie | <MovieName>`

- Uses the movie's key Param in the url to retrieve a specific movie. Went with the key in the URL instead of the ID for the better SEO and it's preferred to not show a DB entity's Key in the client.
- Can see all of a movie's data laid out in this route
- Also added a `trailerKey` to movie model to embed YouTube videos to show the movie's trailer
  - **Note:** Only the top 5 movies have their actual trailer, the rest use the same Deadpool trailer)

### Movie List
- Route: `/movies`
- Breadcrumb items: `[Home]`
- Window title: `Movies`

- Provides the user with a UI to make movie query requests
- Can click on a movie's image or its name to navigate to its detail page

#### Querying Movies
- Uses `MovieQueryRequest` and `QueryResult<Movie>` to handle querying data and receiving results
- `MovieQueryRequest` contains the data used to make a query request for movies (pageNum, pageSize, sortBy, isSortAsc, name, genre)
- `QueryResult<T>` contains the current data that will be displayed and the total number of results of a given query
- Went with this approach as a movies site will generally have thousands of movies at least and such large data sets should be queried for and not requested all together in a single collection
- A new request is made whenever a search is initiated, a page number or page size changes or ordering is changed

#### List Filtering
- Can search for movies by their name (using startsWith approach) and genre (if genre is included in a movie's genre list)
- Can reset search to its initial state (Set in the redux store)
- New results animation: `FadeIn`

#### List Ordering
- Can order results by **name** or by **route** in both Asc/Desc order
- Default ordering set to **name** in **Asc** order
- Ordering by **name** will first order in **Asc order**, while ordering by **route** will first order in **Desc order** (so show highest rated first) for better usability
- Ordered results animation: `FadeIn`

#### List Paging
- Can page through data using the paginator at the bottom
- Can select the number of results shown within each page
- Page forward animation: `FadeInLeft`
- Page backward animation: `FadeInRight`

#### State Management
- Persistence: `SessionStorage`
- Movie Reducer: `movie-list.reducers.ts`
- Movie Actions: `movie-list.actions.ts`
- Saving state for the most recent `MovieQueryRequest` that is completed, for the most recent `QueryResult<Movie>` received and the user's sort options whenever changed
- If the user refreshes the page or navigates to `/movies` using the navbar or the breadcrumb (or browser Back button) then they will see the screen exactly as they left it

#### URL Query Params
- The URL is updated with the `MovieQueryRequest` after each request completes
- Allows for users to share search results with each other

#### Initial data request
- The first request for movie data is made by this order of priority:
	- Use the `QueryResult<Movie>` saved in the store (No request needs to be made here)
	- Use Query Params set in the url to make the initial request (So if another user sends you a link with search query params, that query will be initiated first)
	- Uses the initial `MovieQueryRequest` set inside the redux store
	
#### Animations
- Using Angular animations along with the package `ng-animate`
  - Demo: https://jiayihu.github.io/ng-animate/
  - Npm: https://www.npmjs.com/package/ng-animate
- Route change is animated using a fadeIn effect (See: `shell.animations.ts`)
- Movie list query results are animated as specified in the movie list section (See: `movie-list.animations.ts`)

#### Responsiveness
- Each route is responsive.
- Mostly using flexbox, but also used bootstrap grid system in the footer.