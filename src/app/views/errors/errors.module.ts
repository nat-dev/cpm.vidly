import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';

import { ErrorsRoutingModule } from './errors-routing.module';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
    declarations: [NotFoundComponent],
    imports: [
        SharedModule,
        ErrorsRoutingModule
    ]
})
export class ErrorsModule { }
