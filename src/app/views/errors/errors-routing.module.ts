import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppRouteData } from '@app/core/route/app-route-data.model';

import { BreadcrumbItem } from '@app/shared/models/breadcrumb-item.model';

import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
    {
        path: '404', component: NotFoundComponent,
        data: new AppRouteData({
            title: '404', heading: 'Not Found',
            breadcrumbItems: [new BreadcrumbItem('Home', '/home')]
        })
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ErrorsRoutingModule { }
