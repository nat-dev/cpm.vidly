import { Movie } from '@app/shared/models/movie.interface';
import { QueryResult } from '@app/shared/models/query-result.interface';

import { Action } from '@ngrx/store';

import { MovieQueryRequest } from '../../../../shared/models/movie-query-request.interface';
import { SortItem } from '../models/sort-item.interface';

export const SET_MOVIES_QUERY_REQUEST = 'SET_MOVIES_QUERY_REQUEST';
export const SET_MOVIES_QUERY_RESULT = 'SET_MOVIES_QUERY_RESULT';

export const SET_ORDERING = 'SET_ORDERING';

export class SetMoviesQueryRequest implements Action {
    readonly type = SET_MOVIES_QUERY_REQUEST;

    constructor(public payload: MovieQueryRequest) { }
}

export class SetMoviesQueryResult implements Action {
    readonly type = SET_MOVIES_QUERY_RESULT;

    constructor(public payload: QueryResult<Movie>) { }
}

export class SetOrdering implements Action {
    readonly type = SET_ORDERING;

    constructor(public payload: { [key: string]: SortItem }) { }
}

export type MovieListActions =
    SetMoviesQueryRequest | SetMoviesQueryResult |
    SetOrdering;
