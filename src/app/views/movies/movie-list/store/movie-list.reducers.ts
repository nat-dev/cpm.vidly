import { Movie, OrderableMovieKey } from '@app/shared/models/movie.interface';
import { QueryResult } from '@app/shared/models/query-result.interface';

import { MovieQueryRequest } from '../../../../shared/models/movie-query-request.interface';
import { SortItem } from '../models/sort-item.interface';
import * as MovieListActions from './movie-list.actions';

export interface AppState {
    movieList: State;
}

export interface State {
    moviesQueryRequest: MovieQueryRequest;
    moviesQueryResult: QueryResult<Movie>;
    sortItems: { [key: string]: SortItem };
}

export const initialState: State = {
    moviesQueryRequest: {
        pageNumber: 0, pageSize: 6,
        sortBy: OrderableMovieKey.name,
        isSortAscending: true,
        name: null, genre: null
    },
    moviesQueryResult: { items: [], resultsCount: 0 },
    sortItems: {
        [OrderableMovieKey.name]: { isSortAsc: true, isInitialSortAsc: true },
        [OrderableMovieKey.rate]: { isSortAsc: true, isInitialSortAsc: false }
    }
};

function getState(): State {
    return JSON.parse(sessionStorage.getItem('movieListState')) || initialState;
}

export function movieListReducer(state: State = getState(), action: MovieListActions.MovieListActions) {
    switch (action.type) {
        case MovieListActions.SET_MOVIES_QUERY_REQUEST: {
            const newState = {
                ...state,
                moviesQueryRequest: action.payload
            };
            // Better approach would be to use ngrx-store-localstorage package or add custom middleware
            sessionStorage.setItem('movieListState', JSON.stringify(newState));
            return newState;
        }
        case MovieListActions.SET_MOVIES_QUERY_RESULT: {
            const newState = {
                ...state,
                moviesQueryResult: action.payload
            };
            sessionStorage.setItem('movieListState', JSON.stringify(newState));
            return newState;
        }
        case MovieListActions.SET_ORDERING: {
            const newState = {
                ...state,
                sortItems: action.payload
            };
            sessionStorage.setItem('movieListState', JSON.stringify(newState));
            return newState;
        }
        default:
            return state;
    }
}
