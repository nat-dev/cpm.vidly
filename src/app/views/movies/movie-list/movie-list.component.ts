import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MovieGenreService } from '@app/core/services/movie-genre.service';
import { MovieService } from '@app/core/services/movie.service';
import { UtilityService } from '@app/core/services/utility.service';

import { GenreType } from '@app/shared/models/genre-type.enum';
import { Movie } from '@app/shared/models/movie.interface';
import { QueryResult } from '@app/shared/models/query-result.interface';

import { select, Store } from '@ngrx/store';

import { MovieQueryRequest } from '../../../shared/models/movie-query-request.interface';
import { MovieListAnimations } from './movie-list.animations';
import * as MovieListActions from './store/movie-list.actions';
import * as fromMovieList from './store/movie-list.reducers';

import { Observable } from 'rxjs/internal/Observable';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { of } from 'rxjs/internal/observable/of';

@Component({
    selector: 'app-movie-list',
    templateUrl: './movie-list.component.html',
    styleUrls: ['./movie-list.component.scss'],
    animations: [MovieListAnimations.listChange]
})
export class MovieListComponent implements OnInit {

    /** Stores the the most recent movies query request. */
    private moviesQueryRequest: MovieQueryRequest;

    /** Stores the most recent request's results */
    public moviesQueryResult: QueryResult<Movie>;

    /** Movie genres that are available as movie filtering options in the search form's select component. */
    public genres: GenreType[];

    public listChangeAnimState: 'pageBackward' | 'pageForward' | 'sort' | 'search';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private utilService: UtilityService,
        private movieService: MovieService,
        private movieGenreService: MovieGenreService,
        private store: Store<fromMovieList.AppState>
    ) { }

    public ngOnInit(): void {
        this.initStore();
        this.initRequests();
    }

    private initStore(): void {
        this.store.pipe(select(s => s.movieList.moviesQueryRequest)).subscribe(
            queryRequest => {
                this.moviesQueryRequest = queryRequest;
                this.setRouteQueryParams(this.moviesQueryRequest);
            }
        );

        this.store.pipe(select(s => s.movieList.moviesQueryResult)).subscribe(
            queryResult => { this.moviesQueryResult = queryResult; }
        );
    }

    private initRequests(): void {
        const genres$ = this.movieGenreService.getAll();

        let queryResult$: Observable<QueryResult<Movie>> = of(null);
        let queryRequest: MovieQueryRequest = this.moviesQueryRequest;

        /**
         * We get the initial data set by this order of priority:
         * 1. Use QueryResult in Store (No request has to be made)
         * 2. Use SearchQueryParams in the route to make the request
         * 3. Use initial MovieQueryRequest set in Store to make the request
         */
        if (this.moviesQueryResult.items.length === 0) {
            if (this.route.snapshot.queryParamMap.keys.length > 0) {
                /* MovieQuery saved in search params, for sharing results with other users */
                queryRequest = JSON.parse(JSON.stringify(this.route.snapshot.queryParams), (key, value) => {
                    if (!isNaN(value)) {
                        return +value; // - Convert to type 'number'
                    }
                    if (typeof (value) === 'string' &&
                        (value.toLowerCase() === 'true' || value.toLowerCase() === 'false')
                    ) {
                        return JSON.parse(value); // - Convert to type 'boolean'
                    }
                    return value;
                });
                queryResult$ = this.movieService.queryMovies(queryRequest);
            } else {
                /* The Default Initial MovieQuery Requested */
                queryResult$ = this.movieService.queryMovies(queryRequest);
            }
        } else {
            this.store.dispatch(new MovieListActions.SetMoviesQueryRequest(queryRequest));
        }

        /** Request the movie genres and make the initial movie query request (if needed) */
        forkJoin(genres$, queryResult$).subscribe(
            ([genres, queryResult]) => {
                this.genres = genres;
                if (queryResult) {
                    this.handleQueryResult(queryRequest, queryResult);
                }
            }
        );
    }

    /** Makes a request to query movies */
    private requestMovieQuery(queryRequest: MovieQueryRequest): void {
        const fullQuery: MovieQueryRequest = { ...this.moviesQueryRequest, ...queryRequest };
        this.movieService.queryMovies(fullQuery).subscribe(
            queryResult => {
                this.handleQueryResult(fullQuery, queryResult);
            }
        );
    }

    public onPageChange(queryRequest: MovieQueryRequest): void {
        if (queryRequest.pageNumber >= this.moviesQueryRequest.pageNumber) {
            this.listChangeAnimState = 'pageForward';
        } else {
            this.listChangeAnimState = 'pageBackward';
        }

        this.requestMovieQuery(queryRequest);
    }

    public onSortChange(queryRequest: MovieQueryRequest): void {
        this.listChangeAnimState = 'sort';
        this.requestMovieQuery(queryRequest);
    }

    public onSearch(queryRequest: MovieQueryRequest): void {
        this.listChangeAnimState = 'search';
        this.requestMovieQuery(queryRequest);
    }

    /** Actions taken after query search results are returned */
    private handleQueryResult(queryRequest: MovieQueryRequest, queryResult: QueryResult<Movie>): void {
        this.store.dispatch(new MovieListActions.SetMoviesQueryRequest(queryRequest));
        this.store.dispatch(new MovieListActions.SetMoviesQueryResult(queryResult));

        this.utilService.scrollToTop();
    }

    /** Sets movie request as search query params */
    private setRouteQueryParams(queryRequest: MovieQueryRequest): void {
        this.router.navigate(['.'], { queryParams: queryRequest, relativeTo: this.route });
    }

}
