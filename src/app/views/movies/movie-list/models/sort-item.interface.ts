export interface SortItem {

    /** Current sort ordering being used */
    isSortAsc: boolean;

    /** Sort ordering used when ordering results by a new key */
    isInitialSortAsc: boolean;

}
