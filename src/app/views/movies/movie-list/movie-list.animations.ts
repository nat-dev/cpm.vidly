import { transition, trigger, useAnimation } from '@angular/animations';

import { fadeIn, fadeInLeft, fadeInRight } from 'ng-animate';

export class MovieListAnimations {

    public static listChange = trigger('listChange', [
        transition('* => pageForward', [
            useAnimation(fadeInLeft, { params: { timing: 0.6 } })
        ]),
        transition('* => pageBackward', [
            useAnimation(fadeInRight, { params: { timing: 0.6 } })
        ]),
        transition('* => sort, * => search', [
            useAnimation(fadeIn, { params: { timing: 0.6 } })
        ])
    ]);

}
