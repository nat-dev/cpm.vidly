import { Component, Input } from '@angular/core';

import { BaseMovieCard } from '@app/shared/components/_base/base-movie-card.component';
import { Movie } from '@app/shared/models/movie.interface';

@Component({
    selector: 'app-movie-list-card',
    templateUrl: './movie-list-card.component.html',
    styleUrls: ['./movie-list-card.component.scss']
})
export class MovieListCardComponent extends BaseMovieCard {

    @Input() public movie: Movie;

}
