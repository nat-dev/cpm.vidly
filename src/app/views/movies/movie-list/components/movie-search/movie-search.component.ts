import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { GenreType } from '@app/shared/models/genre-type.enum';

import { select, Store } from '@ngrx/store';

import { MovieQueryRequest } from '../../../../../shared/models/movie-query-request.interface';
import * as fromMovieList from '../../store/movie-list.reducers';

@Component({
    selector: 'app-movie-search',
    templateUrl: './movie-search.component.html',
    styleUrls: ['./movie-search.component.scss']
})
export class MovieSearchComponent implements OnInit {

    @Output() searchSubmit = new EventEmitter<MovieQueryRequest>();

    @Input() genres: GenreType[];

    private moviesQueryRequest: MovieQueryRequest;

    public searchForm: FormGroup;

    constructor(private store: Store<fromMovieList.AppState>) { }

    public ngOnInit(): void {
        this.initStore();
        this.initForm();
    }

    private initStore(): void {
        this.store.pipe(select(s => s.movieList.moviesQueryRequest)).subscribe(
            queryRequest => { this.moviesQueryRequest = queryRequest; }
        );
    }

    private initForm(): void {
        this.searchForm = new FormGroup({
            name: new FormControl(this.moviesQueryRequest.name),
            genre: new FormControl(this.moviesQueryRequest.genre)
        });
    }

    public onSubmit(): void {
        const [movieName, movieGenre] = [this.searchForm.value.name, this.searchForm.value.genre];
        const filterQuery: MovieQueryRequest = {
            pageNumber: 0, name: movieName, genre: movieGenre
        };

        this.searchSubmit.emit(filterQuery);
    }

    /** Resets the search results to the initial state */
    public onResetResults(): void {
        this.searchForm.reset();
        const resetQuery = fromMovieList.initialState.moviesQueryRequest;
        this.searchSubmit.emit(resetQuery);
    }

}
