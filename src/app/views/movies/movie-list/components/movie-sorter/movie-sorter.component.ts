import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { OrderableMovieKey } from '@app/shared/models/movie.interface';

import { select, Store } from '@ngrx/store';

import { MovieQueryRequest } from '../../../../../shared/models/movie-query-request.interface';
import { SortItem } from '../../models/sort-item.interface';
import * as MovieListActions from '../../store/movie-list.actions';
import * as fromMovieList from '../../store/movie-list.reducers';

@Component({
    selector: 'app-movie-sorter',
    templateUrl: './movie-sorter.component.html',
    styleUrls: ['./movie-sorter.component.scss']
})
export class MovieSorterComponent implements OnInit {

    @Output() sortChange = new EventEmitter<MovieQueryRequest>();

    public orderableKeys = OrderableMovieKey;

    /** Current key being used to sort results. */
    public activeSort: string;

    private sortItems: { [key: string]: SortItem };

    constructor(private store: Store<fromMovieList.AppState>) { }

    public ngOnInit(): void {
        this.store.pipe(select(s => s.movieList.sortItems)).subscribe(
            sortItems => { this.sortItems = sortItems; }
        );

        this.store.pipe(select(s => s.movieList.moviesQueryRequest)).subscribe(
            queryRequest => {
                this.activeSort = queryRequest.sortBy;
                this.sortItems[this.activeSort].isSortAsc = queryRequest.isSortAscending;
            }
        );
    }

    /** Sort option clicked */
    public onSortMovies(key: OrderableMovieKey): void {
        this.sortItems[key].isSortAsc = !this.sortItems[key].isSortAsc;

        if (this.activeSort !== key) {
            this.sortItems[key].isSortAsc = this.sortItems[key].isInitialSortAsc;
        }

        this.store.dispatch(new MovieListActions.SetOrdering(this.sortItems));

        const sortQuery: MovieQueryRequest = {
            pageNumber: 0, sortBy: key, isSortAscending: this.sortItems[key].isSortAsc
        };

        this.sortChange.emit(sortQuery);
    }

    /** Returns the font-awesome icon being used for a sort item */
    public getSortIcon(key: OrderableMovieKey): string {
        let iconName: string;
        /* Set font-awesome icon names explicitly for each key, or can use sort-amount by default */
        switch (key) {
            case this.orderableKeys.name:
                iconName = 'sort-alpha-';
                break;
            case this.orderableKeys.rate:
                iconName = 'sort-numeric-';
                break;
            default:
                iconName = 'sort-amount-'; // - Note: Import this in font-awesome library if going to use!
        }

        let currentSortAsc = this.sortItems[key].isSortAsc;
        /* Use initialSortAsc value for ordering if ordering results using a different key */
        if (this.activeSort !== key) {
            currentSortAsc = this.sortItems[key].isInitialSortAsc;
        }

        const direction = currentSortAsc ? 'down' : 'up';
        return `${iconName}${direction}`;
    }

}
