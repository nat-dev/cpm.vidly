import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { MovieQueryRequest } from '../../../../../shared/models/movie-query-request.interface';
import * as fromMovieList from '../../store/movie-list.reducers';

@Component({
    selector: 'app-movie-paginator',
    templateUrl: './movie-paginator.component.html',
    styleUrls: ['./movie-paginator.component.scss']
})
export class MoviePaginatorComponent implements OnInit {

    @Output() pageIndexChange = new EventEmitter<MovieQueryRequest>();
    @Output() pageSizeChange = new EventEmitter<MovieQueryRequest>();

    /** Current page number */
    public pageIndex: number;

    /** Total number of results of movie query. Used to know how many pages to display. */
    private totalQueryResults: number;

    /** Number of movie items per page. */
    public pageSize: number;

    /** Page size options available to the user. Even numbers work best with the list layout. */
    public pageSizeOptions = [4, 6, 8, 10, 12];

    /** The number of data pages based off the total number of movies divided by the page size. */
    public get numberOfPages(): number {
        return Math.ceil(this.totalQueryResults / this.pageSize);
    }

    /** Returns an array of numbers (0...NumberOfPages) for using *ng-For with paginator page-items. */
    public get paginatorPages(): number[] {
        return Array.from(Array(this.numberOfPages).keys());
    }

    /** If can page backwards. */
    public get canPrevPage(): boolean {
        return this.pageIndex > 0;
    }

    /** If can page forward. */
    public get canNextPage(): boolean {
        return this.pageIndex < (this.numberOfPages - 1);
    }

    constructor(private store: Store<fromMovieList.AppState>) { }

    public ngOnInit(): void {
        this.store.pipe(select(s => s.movieList.moviesQueryRequest)).subscribe(
            queryRequest => {
                this.pageSize = queryRequest.pageSize;
                this.pageIndex = queryRequest.pageNumber;
            }
        );

        this.store.pipe(select(s => s.movieList.moviesQueryResult)).subscribe(
            queryResult => { this.totalQueryResults = queryResult.resultsCount; }
        );
    }

    /** Paginator item (page number, previous, next) clicked */
    public onPageIndexChange(pageNumber: number): void {
        const pageIndexQuery: MovieQueryRequest = { pageNumber: pageNumber };
        this.pageIndexChange.emit(pageIndexQuery);
    }

    /** Page size select input option selected */
    public onPageSizeChange(): void {
        const pageSizeQuery: MovieQueryRequest = { pageSize: this.pageSize };
        if (this.pageIndex > this.numberOfPages) {
            pageSizeQuery.pageNumber = this.numberOfPages - 1;
        }

        this.pageSizeChange.emit(pageSizeQuery);
    }

}
