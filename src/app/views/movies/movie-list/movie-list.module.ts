import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@app/shared/shared.module';

import { MovieListComponent } from './movie-list.component';
import { MovieSearchComponent } from './components/movie-search/movie-search.component';
import { MoviePaginatorComponent } from './components/movie-paginator/movie-paginator.component';
import { MovieSorterComponent } from './components/movie-sorter/movie-sorter.component';
import { MovieListCardComponent } from './components/movie-list-card/movie-list-card.component';

@NgModule({
    declarations: [
        MovieListComponent,
        MovieSearchComponent,
        MoviePaginatorComponent,
        MovieSorterComponent,
        MovieListCardComponent
    ],
    imports: [
        SharedModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule
    ]
})
export class MovieListModule { }
