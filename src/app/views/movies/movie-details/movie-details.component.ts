import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MovieService } from '@app/core/services/movie.service';
import { ShellService } from '@app/core/services/shell.service';

import { BaseMovieCard } from '@app/shared/components/_base/base-movie-card.component';

@Component({
    selector: 'app-movie-details',
    templateUrl: './movie-details.component.html',
    styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent extends BaseMovieCard implements OnInit {

    public get movieTrailerSrc(): string {
        return `https://www.youtube.com/embed/${this.movie.trailerKey}`;
    }

    constructor(
        private route: ActivatedRoute,
        private movieService: MovieService,
        private shellService: ShellService
    ) {
        super();
    }

    public ngOnInit() {
        this.route.params.subscribe(
            params => {
                const movieKey = params['key'];
                this.movieService.getByKey(movieKey).subscribe(
                    movie => {
                        this.movie = movie;
                        this.shellService.heading = this.movie.name;
                        this.shellService.windowTitle = `Movie | ${this.movie.name}`;
                    }
                );
            }
        );
    }

}
