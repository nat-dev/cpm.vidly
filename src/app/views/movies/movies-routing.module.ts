import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppRouteData } from '@app/core/route/app-route-data.model';

import { BreadcrumbItem } from '@app/shared/models/breadcrumb-item.model';

import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MovieListComponent } from './movie-list/movie-list.component';


const routes: Routes = [
    {
        path: '', component: MovieListComponent,
        data: new AppRouteData({
            title: 'Movies', heading: 'Movies',
            breadcrumbItems: [new BreadcrumbItem('Home', '/home')]
        })
    },
    {
        path: ':key', component: MovieDetailsComponent,
        data: new AppRouteData({
            breadcrumbItems: [
                new BreadcrumbItem('Home', '/home'),
                new BreadcrumbItem('Movies', '/movies'),
            ]
        })
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MoviesRoutingModule { }
