import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';

import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MovieListModule } from './movie-list/movie-list.module';
import { MoviesRoutingModule } from './movies-routing.module';

@NgModule({
    declarations: [
        MovieDetailsComponent
    ],
    imports: [
        SharedModule,
        MovieListModule,
        MoviesRoutingModule
    ]
})
export class MoviesModule { }
