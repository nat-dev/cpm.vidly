import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@app/shared/shared.module';

import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { HomeComponent } from './home.component';

@NgModule({
    declarations: [
        HomeComponent,
        MovieCardComponent
    ],
    imports: [
        SharedModule,
        RouterModule
    ]
})
export class HomeModule { }
