import { Component, OnInit } from '@angular/core';

import { MovieService } from '@app/core/services/movie.service';

import { MovieQueryRequest } from '@app/shared/models/movie-query-request.interface';
import { Movie, OrderableMovieKey } from '@app/shared/models/movie.interface';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    public topMovies: Movie[];

    constructor(
        private movieService: MovieService
    ) { }

    public ngOnInit() {
        const topFiveRatedQuery: MovieQueryRequest = {
            pageNumber: 0, pageSize: 5,
            sortBy: OrderableMovieKey.rate, isSortAscending: false
        };

        this.movieService.queryMovies(topFiveRatedQuery).subscribe(
            queryResult => {
                this.topMovies = queryResult.items;
            }
        );
    }

}
