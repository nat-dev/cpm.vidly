import { Component, Input } from '@angular/core';

import { BaseMovieCard } from '@app/shared/components/_base/base-movie-card.component';
import { Movie } from '@app/shared/models/movie.interface';

@Component({
    selector: 'app-movie-card',
    templateUrl: './movie-card.component.html',
    styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent extends BaseMovieCard {
    @Input() public movie: Movie;
}
