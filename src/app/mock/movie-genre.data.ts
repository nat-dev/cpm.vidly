import { GenreType } from '@app/shared/models/genre-type.enum';

export const movieGenres: GenreType[] = Object.values(GenreType);
