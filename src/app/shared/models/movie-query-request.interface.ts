import { GenreType } from '@app/shared/models/genre-type.enum';
import { QueryRequest } from '@app/shared/models/query-request.interface';

export interface MovieQueryRequest extends QueryRequest {

    /** Request movies by name. Uses 'startsWith' approach. */
    name?: string;

    /** Request movies by genre. Checks if genre is within the movies genres list. */
    genre?: GenreType;

}
