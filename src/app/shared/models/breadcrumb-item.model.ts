export class BreadcrumbItem {

    constructor(
        /** Value to display. */
        public title: string,
        /** RouterLink for navigation. */
        public routerLink: any[] | string
    ) { }

}
