export interface QueryResult<T> {
    items: T[];
    resultsCount: number;
}
