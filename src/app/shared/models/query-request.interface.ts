export interface QueryRequest {

    /** Index of data page to return. Starts from 0. */
    pageNumber?: number;

    /** Number of results to return within a data page. */
    pageSize?: number;

    /** Sort results by a property key */
    sortBy?: string;

    /** If sort should be in ascending order or not. */
    isSortAscending?: boolean;

}
