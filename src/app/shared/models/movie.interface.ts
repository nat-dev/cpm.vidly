import { GenreType } from './genre-type.enum';

/** Keys of Movie model that are sortable */
export enum OrderableMovieKey {
    name = 'name',
    rate = 'rate'
}

export interface Movie {
    id: number;
    key: string;
    [OrderableMovieKey.name]: string;
    description: string;
    genres: GenreType[];
    [OrderableMovieKey.rate]: string;
    length: string;
    img: string;
    trailerKey: string;
}
