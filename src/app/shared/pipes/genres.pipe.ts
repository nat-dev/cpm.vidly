import { Pipe, PipeTransform } from '@angular/core';

import { GenreType } from '../models/genre-type.enum';

@Pipe({
    name: 'genres'
})
export class GenresPipe implements PipeTransform {

    public transform(genres: GenreType[]): any {
        return genres.join(', ');
    }

}
