import { Movie } from '@app/shared/models/movie.interface';

export class BaseMovieCard {
    public movie: Movie;

    public get movieImgSrc(): string {
        return `/assets/images/movie-covers/${this.movie.img}`;
    }

    public get movieImgAlt(): string {
        return `${this.movie.name} Movie Cover Image`;
    }
}
