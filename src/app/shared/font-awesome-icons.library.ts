import { library } from '@fortawesome/fontawesome-svg-core';
import { faFacebook, faGooglePlusG, faTwitter } from '@fortawesome/free-brands-svg-icons';
import {
    faAngleRight,
    faClock,
    faSort,
    faSortAlphaDown,
    faSortAlphaUp,
    faSortNumericDown,
    faSortNumericUp,
    faStar,
} from '@fortawesome/free-solid-svg-icons';

/*
 * See: https://www.npmjs.com/package/@fortawesome/angular-fontawesome#using-the-icon-library
 */

library.add(
    /* fab */
    faFacebook, faGooglePlusG, faTwitter,

    /* fas */
    faAngleRight,
    faClock,
    faSort,
    faSortAlphaDown,
    faSortAlphaUp,
    faSortNumericDown,
    faSortNumericUp,
    faStar
);
