import './font-awesome-icons.library';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { GenresPipe } from './pipes/genres.pipe';
import { SafeUrlPipe } from './pipes/safe-url.pipe';

@NgModule({
    declarations: [
        GenresPipe,
        SafeUrlPipe
    ],
    imports: [
        CommonModule,
        FontAwesomeModule
    ],
    exports: [
        CommonModule,
        FontAwesomeModule,
        GenresPipe,
        SafeUrlPipe
    ]
})
export class SharedModule { }
