import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShellHelper } from './core/components/_shell/shell.helper';
import { HomeComponent } from './views/home/home.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },

    ShellHelper.childRoutes([
        { path: 'home', component: HomeComponent, data: { title: 'Home', heading: 'Highest Rated Movies' } },
        { path: 'movies', loadChildren: '@views/movies/movies.module#MoviesModule' },
        { path: 'errors', loadChildren: '@views/errors/errors.module#ErrorsModule' },

        /* Place at end to handle unknown routes as any routes placed under this will be ignored. */
        { path: '**', redirectTo: 'errors/404' }
    ])
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
