import { BreadcrumbItem } from '@app/shared/models/breadcrumb-item.model';

export class AppRouteData {

    public title: string;
    public heading: string;
    public breadcrumbItems: BreadcrumbItem[];

    constructor(config: {
        title?: string,
        heading?: string,
        breadcrumbItems?: BreadcrumbItem[]
    } = {}) {
        this.title = config.title;
        this.heading = config.heading;
        this.breadcrumbItems = config.breadcrumbItems || [];
    }

}
