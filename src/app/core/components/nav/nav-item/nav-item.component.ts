import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-nav-item',
    templateUrl: './nav-item.component.html',
    styleUrls: ['./nav-item.component.scss']
})
export class NavItemComponent {

    /** Used as routerLink value. */
    @Input() navTo: any[] | string;

    /** Class applied to active routes. Use class 'active' by default. */
    @Input() routerLinkActive = 'active';
}
