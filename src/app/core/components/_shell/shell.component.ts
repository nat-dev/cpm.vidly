import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { ShellService } from '@app/core/services/shell.service';

import { BreadcrumbItem } from '@app/shared/models/breadcrumb-item.model';

import { ShellAnimations } from './shell.animations';

@Component({
    selector: 'app-shell',
    templateUrl: './shell.component.html',
    styleUrls: ['./shell.component.scss'],
    animations: [ShellAnimations.routeFadeIn]
})
export class ShellComponent {

    /** The heading text displayed in the H1 between nav and content */
    public get heading(): string {
        return this.shellService.heading;
    }

    /** Breadcrumb links used to navigate backwards. May be set in component route files. */
    public get breadcrumbItems(): BreadcrumbItem[] {
        return this.shellService.breadcrumbItems;
    }

    /**
     * Returns route title if it is activated. Used for route animation.
     * (Route titles used for transitioning between routes, but currently using * <=> * for any routes)
     * @param outletRef RouterOutlet local reference made in template.
     */
    public getRouteTitle(outletRef: RouterOutlet): string {
        return outletRef.isActivated ? outletRef.activatedRouteData.title : '';
    }

    constructor(private shellService: ShellService) { }

}
