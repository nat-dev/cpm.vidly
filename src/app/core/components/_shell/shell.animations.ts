import { query, style, transition, trigger, useAnimation } from '@angular/animations';

import { fadeIn } from 'ng-animate';

export class ShellAnimations {

    public static routeFadeIn = trigger('routeFadeIn', [
        transition('* <=> *', [
            style({ position: 'relative', overflow: 'hidden' }),
            query(':enter', [
                useAnimation(fadeIn, { params: { timing: 0.22 } })
            ])
        ])
    ]);

}
