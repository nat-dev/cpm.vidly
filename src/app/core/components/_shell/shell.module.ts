import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { FooterModule } from '../footer/footer.module';
import { NavModule } from '../nav/nav.module';
import { ShellComponent } from './shell.component';

@NgModule({
    declarations: [ShellComponent],
    imports: [
        CommonModule,
        RouterModule,
        FontAwesomeModule,
        NavModule,
        FooterModule
    ]
})
export class ShellModule { }
