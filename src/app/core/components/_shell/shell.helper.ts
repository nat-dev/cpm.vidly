import { Route, Routes } from '@angular/router';

import { ShellComponent } from './shell.component';

/**
 * Provides helper methods to create routes.
 */
export class ShellHelper {

    /**
     * Creates routes using the shell component.
     * @param routes The routes to add.
     * @return {Route} The new route using shell as the base.
     */
    public static childRoutes(routes: Routes): Route {
        return {
            path: '',
            component: ShellComponent,
            children: routes
            // canActivate: [AuthenticationGuard]
        };
    }

}
