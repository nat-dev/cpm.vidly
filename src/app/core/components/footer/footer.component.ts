import { Component } from '@angular/core';

import { appConstants } from '@app/core/constants/app.constants';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
    appName = appConstants.appName;
}
