import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { FooterComponent } from './footer.component';

@NgModule({
    declarations: [FooterComponent],
    imports: [
        RouterModule,
        FontAwesomeModule
    ],
    exports: [
        FooterComponent
    ]
})
export class FooterModule { }
