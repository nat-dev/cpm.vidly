import { NgModule } from '@angular/core';

import { ShellModule } from './components/_shell/shell.module';

@NgModule({
    imports: [
        ShellModule,
    ]
})
export class CoreModule { }
