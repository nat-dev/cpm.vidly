import { Injectable } from '@angular/core';

import { GenreType } from '@app/shared/models/genre-type.enum';

import { movieGenres } from '@app/mock/movie-genre.data';

import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
    providedIn: 'root'
})
export class MovieGenreService {

    private get genres(): GenreType[] { return movieGenres; }

    public getAll(): Observable<GenreType[]> {
        return of(this.genres.slice());
    }

}
