import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { BreadcrumbItem } from '@app/shared/models/breadcrumb-item.model';

import { appConstants } from '../constants/app.constants';

@Injectable({
    providedIn: 'root'
})
export class ShellService {

    /** H1 heading value between nav and content. */
    public heading: string;

    /** Breadcrumb links displayed above the heading. */
    public breadcrumbItems: BreadcrumbItem[] = [];

    constructor(private titleService: Title) { }

    /** Sets the window title using Title. Includes AppName prefix. */
    public set windowTitle(title: string) {
        this.titleService.setTitle(`${appConstants.appName} | ${title}`);
    }

}
