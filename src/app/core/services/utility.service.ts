import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UtilityService {

    /**
     * Sorts a list using one of its keys.
     * @param list List to sort.
     * @param key Key of object to sort with.
     * @param isAsc If sort order should be in ascending order. True by default.
     */
    public sortListByKey<TList>(list: TList[], key: string, isAsc: boolean = true): TList[] {
        return list.sort(
            (objA, objB) => {
                if (objA[key] > objB[key]) {
                    return isAsc ? 1 : -1;
                }
                if (objA[key] < objB[key]) {
                    return isAsc ? -1 : 1;
                }

                return 0;
            }
        );
    }

    /**
     * Scrolls up to the specified position or to the top of the page if not specified.
     * @param topPos Destination / Top position to scroll to. 0 (Top of page) by default.
     */
    public scrollToTop(topPos: number = 0): void {
        const scrollToTop = window.setInterval(() => {
            const currPos = window.pageYOffset;
            if (currPos > topPos) {
                window.scrollTo(topPos, currPos - 20); // - How far to scroll on each step
            } else {
                window.clearInterval(scrollToTop);
            }
        }, 16);
    }

}
