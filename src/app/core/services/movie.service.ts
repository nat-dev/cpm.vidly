import { Injectable } from '@angular/core';

import { GenreType } from '@app/shared/models/genre-type.enum';
import { MovieQueryRequest } from '@app/shared/models/movie-query-request.interface';
import { Movie } from '@app/shared/models/movie.interface';
import { QueryResult } from '@app/shared/models/query-result.interface';

import { movies } from '@app/mock/movie.mock-data';

import { UtilityService } from './utility.service';

import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
    providedIn: 'root'
})
export class MovieService {

    private get movies(): Movie[] { return movies.slice(); }

    constructor(private utilService: UtilityService) { }

    /**
     * Requests a query to movies and returns a page of data.
     * @param queryRequest Object used to make movie query requests.
     */
    public queryMovies(queryRequest: MovieQueryRequest): Observable<QueryResult<Movie>> {
        const { name, genre, sortBy, isSortAscending, pageNumber, pageSize } = queryRequest;
        let results = this.movies;

        if (name) {
            const sanitizedNamed = name.trim().toLowerCase();
            results = results.filter(m => m.name.toLowerCase().startsWith(sanitizedNamed));
        }

        if (genre) {
            // results = results.map(m => {
            //     m.genres = m.genres.map(g => g.toLowerCase() as GenreType);
            //     return m;
            // });
            results = results.filter(m => m.genres.includes(genre.toLowerCase() as GenreType));
        }

        if (sortBy) {
            results = this.utilService.sortListByKey(results, sortBy, isSortAscending);
        }

        const startIndex = pageNumber * pageSize;
        const result: QueryResult<Movie> = {
            items: results.slice(startIndex, pageSize + startIndex),
            resultsCount: results.length
        };

        return of(result);
    }

    /**
     * Requests a movie by its key.
     * @param key Unique key of movie being requested.
     */
    public getByKey(key: string): Observable<Movie> {
        const movie = this.movies.find(m => m.key === key);
        return of(movie);
    }

}
