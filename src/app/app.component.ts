import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { AppRouteData } from './core/route/app-route-data.model';
import { ShellService } from './core/services/shell.service';

import { filter } from 'rxjs/internal/operators/filter';
import { map } from 'rxjs/internal/operators/map';
import { mergeMap } from 'rxjs/internal/operators/mergeMap';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private shellService: ShellService
    ) { }

    public ngOnInit() {
        const onNavigationEnd$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

        /* Set page title on navigation change using route data */
        onNavigationEnd$.pipe(
            map(() => {
                let route = this.activatedRoute;
                while (route.firstChild) {
                    route = route.firstChild;
                }
                return route;
            }),
            filter(route => route.outlet === 'primary'),
            mergeMap(route => route.data))
            .subscribe(routeData => {
                const {
                    title, heading, breadcrumbItems
                } = { ...new AppRouteData(), ...routeData };

                if (title) {
                    this.shellService.windowTitle = title;
                }

                if (heading) {
                    this.shellService.heading = heading;
                }

                this.shellService.breadcrumbItems = breadcrumbItems || [];
            });
    }
}
